## This project will be updated soon, then language will be changed from C to LISP

# International Standard Atmosphere Calculator

This program calculates the air temperature, pressure and density for an specific hight inputted.

*Height must be in metres*

### What is does

- Calculates Temperature
- Calculates Air Density
- Calculates Air Pressure
- Calculates Geometric Altitude
- Calculates Geopotential Altitude

### Requirements

* cmake
* gcc compiler
* C headers:
    * string.h
    * stdlib.h
* Windows users needs to install [Mingw-w64](https://mingw-w64.org/doku.php)

### Usage

Run make in the same directory of Makefile
then run ./calculator Cisa
    
    
    make
    ./calculator Cisa
    

You will be prompted to enter the height, in metres, then It's will provide the Temperature, Pressure, Density, Geopotencial Altitude, and Geometric Altitude

    
    Enter the height in Metres: 11000
    
    Temperature:[Kelvin]: 216.650000 [Celcius]: -56.500000
    The pressure in this altitude is: 22631.700909927 Pa
    The air density is: 0.363915833 Kg/m³
    The geopotencial height is: 0.000000000 metres
    The geometric height is: 0.000000000

    

## TODO
* Implement Exponential Function (not use pow from  math.h)
* Calculate Lift
    * Cl Calculation (Lift Coeficient)
* Calculate Drag
    * Cd Calculation(Drag Coeficient)
* Specify the requirements for running the app
* Update the LICENSE
* Further TODO:
    - Calculate dh
    - Calculate dt
    - other formulas
* BUGS:
   

## Built with

* C

## Author

* **Lucas Resende Goes**

* *Email: devlwkyan@protonmail.com*
## License

This project is licensed under the GPLv3 License - see the [LICENSE](LICENSE) file for details
