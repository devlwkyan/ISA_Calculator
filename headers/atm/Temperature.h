/*
    ISA Calculator, performs aerodynamics computations
    Copyright (C) 2019 Lucas Resende Goes
    
    This file is part of ISA Calculator

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/



#include <stdio.h>
#include "Pressure.h"
const double

            CONST_T = 288.15, // Constant Temperature in Kelvin
            CONST_R = 287.05, // Universal Gas Constant [J/(mol*K)]
            CONST_G_ACC = 9.80665, // Constant gravity acceleration
            STANDARD_PRESSURE_SEA_LEVEL = 101325, // Standard pressure at sea level in Pascal
            A_0_TO_11 = -0.0065,
            A_11_TO_20 = 0,
            A_20_TO_32 = 0.0010,
            A_32_TO_47 = 0.0028,
            A_47_TO_51 = 0,
            A_51_TO_71 = -0.0028,
            A_71_TO_84852 = -0.0020,
            A_85_PLUS = 0;

double Temperature(double height_target){
    double t_target; 
    //up to 11000
    if(height_target <= 11000){
        t_target = CONST_T + A_0_TO_11*(height_target);
        printf("Temperature:[Kelvin]: %.6f [Celcius]: %.6f\n", t_target, t_target-273.15);
        Pressure(0, A_0_TO_11, t_target, CONST_T, STANDARD_PRESSURE_SEA_LEVEL, 0, 0, CONST_R, CONST_G_ACC);
    }
    //up to 20000 -> Isothermic
    else if(height_target >= 11000 && height_target <= 20000){
        t_target = 216.65 + A_11_TO_20*(height_target - 11000);
        printf("Temperature:[Kelvin]: %.6f [Celcius]: %.6f\n", t_target, t_target-273.15);
        Pressure(1, A_11_TO_20, t_target, 0, 22631.70090993, height_target, 11000, CONST_R, CONST_G_ACC);
    }
    //up to 32000
    else if (height_target >=20000 && height_target <= 32000){
        t_target = 216.65 + A_20_TO_32*(height_target - 20000);
        printf("Temperature:[Kelvin]: %.6f [Celcius]: %.6f\n", t_target, t_target-273.15);
        Pressure(0, A_20_TO_32, t_target, 216.65, 5474.71768858, height_target, 20000, CONST_R, CONST_G_ACC);       
    }
    //up to 47000
    else if (height_target >=32000 && height_target <= 47000){
        t_target = 228.65 + A_32_TO_47*(height_target - 32000);
        printf("Temperature:[Kelvin]: %.6f [Celcius]: %.6f\n", t_target, t_target-273.15);
        Pressure(0, A_32_TO_47, t_target, 228.65, 867.97446830, height_target, 32000, CONST_R, CONST_G_ACC); 
    }
    //up to 51000 -> Isothermic
    else if (height_target >=47000 && height_target <= 51000){
        t_target = 270.65 + A_47_TO_51*(height_target - 47000);
        printf("Temperature:[Kelvin]: %.6f [Celcius]: %.6f\n", t_target, t_target-273.15);
        Pressure(1, A_47_TO_51, t_target, 0, 110.89821404, height_target, 47000, CONST_R, CONST_G_ACC); 
    }
    //up to 71000
    else if (height_target >=51000 && height_target <= 71000){
        t_target = 270.65 + A_51_TO_71*(height_target - 51000);
        printf("Temperature:[Kelvin]: %.6f [Celcius]: %.6f\n", t_target, t_target-273.15);
        Pressure(0, A_51_TO_71, t_target, 270.65, 66.93362770, height_target, 51000, CONST_R, CONST_G_ACC);        
    }
    //up to 84852
    else if (height_target >=71000 && height_target <= 84852){
        t_target = 259.45 + A_71_TO_84852*(height_target - 71000);
        printf("Temperature:[Kelvin]: %.6f [Celcius]: %.6f\n", t_target, t_target-273.15);
        Pressure(0, A_71_TO_84852, t_target, 214.65, 3.95599065, height_target, 71000, CONST_R, CONST_G_ACC);
    }
    //85000 or more -> isothermic <not precise>
    else{
        printf("Calculatiosn at this hight is not precise, use as base but do not trust those calculations:\n");
        t_target = 259.45 + A_85_PLUS*(height_target - 84852);
        printf("Temperature:[Kelvin]: %.6f [Celcius]: %.6f\n", t_target, t_target-273.15);
        Pressure(1, A_85_PLUS, t_target, 0, 231.74600, height_target, 84852, CONST_R, CONST_G_ACC);
    }
    return 0;
}
