/*
    ISA Calculator, performs aerodynamics computations
    Copyright (C) 2019 Lucas Resende Goes
    
    This file is part of ISA Calculator

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/



#include <stdio.h>
double Density(double pressure, double temperature, double CONST_R){
    double rho = pressure/(temperature*CONST_R);
    printf("The air density is: %.9lf Kg/m³\n", rho);
    return 0;
}
