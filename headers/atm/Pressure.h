/*
    ISA Calculator, performs aerodynamics computations
    Copyright (C) 2019 Lucas Resende Goes
    
    This file is part of ISA Calculator

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <stdio.h>
#include <math.h>
#include "Density.h"
#include "GeoAlt.h"

const double E_CONST = 2.71828182845904523536; // e
double Pressure(int iso, double a,double t_final, double t_initial, double previous_pressure, double height_final, double height_initial, double CONST_R, double CONST_G_ACC){
    double press_final;    
    if (iso==0){
        press_final = pow(t_final/t_initial, -(CONST_G_ACC/(a*CONST_R))) * previous_pressure;
        printf("The pressure in this altitude is: %.9f Pa\n", press_final); 
        Density(press_final, t_final, CONST_R);
        GeoAlt(height_final);
    }
    else if(iso==1){
        press_final = pow(E_CONST, -((CONST_G_ACC/(CONST_R*t_final))*(height_final-height_initial))) * previous_pressure;
        printf("The pressure in this altitiud is: %.9f Pa\n", press_final);
        Density(press_final, t_final, CONST_R);
        GeoAlt(height_final);
    }
   return 0;
}
