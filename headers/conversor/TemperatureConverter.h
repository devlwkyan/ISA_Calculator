/*
    ISA Calculator, performs aerodynamics computations
    Copyright (C) 2019 Lucas Resende Goes
    
    This file is part of ISA Calculator

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/



#include <stdio.h>

double KelvinToCelcius (double kelvin) {
    double celcius = kelvin - 273.15;
    printf("The temperature [%.3lf]K in Celcius is: %.3lfºC\n", kelvin, celcius);
}

double KelvinToFarenheit (double kelvin) {
    double farenheit = (kelvin-273.15)*1.8000 + 32.00;
    printf("The temperature [%.3lf]K in Farenheit is: %3.lfºF\n", kelvin, farenheit);
}

double CelciusToKelvin (double celcius) {
    double kelvin = celcius + 273.15;
    printf("The temperature [%.3lf]ºC in Kelvin is: %.3lfK\n", celcius, kelvin );
}

double CelciusToFarenheit (double celcius) {
    double farenheit = celcius*1.8000 + 32.00; 
    printf("The temperature [%.3lf]ºC in Farenheit is: %.3lfºF\n", celcius, farenheit);
}

double FarenheitToKelvin (double farenheit) {
    double kelvin = ((farenheit-32)/1.8000) + 273.15;
    printf("The temperature [%.3lf]ºF in Kelvin is: %.3lfK\n", farenheit, kelvin);
}

double FarenheitToCelcius (double farenheit) {
    double celcius = (farenheit-32)/1.8000;
    printf("The temperature [%.3lf]ºF in Celcius is: %.3lfºC\n", farenheit, celcius);
}
